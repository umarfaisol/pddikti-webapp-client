<%@include file="/util/import.jsp" %>

<%
String token = ( session.getAttribute("token") == null ? "" : session.getAttribute("token").toString() );
String endpoint = ( session.getAttribute("endpoint") == null ? "" : session.getAttribute("endpoint").toString() );
String act = ( request.getParameter("act") == null ? "" : request.getParameter("act") );

if (token.equals("")) {
    response.sendRedirect(request.getContextPath() + "/");
}

/* set default endpoint */
if (endpoint.equals("")) {
    endpoint = "http://localhost:8082/ws/live.php";
}
%>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PD-Dikti Feeder Client | <%= token %></title>

        <%-- css --%>
        <%@include file="/util/css.jsp" %>
  </head>
  <body style="margin-left:20px;margin-right:20px;">
    <div class="page-header">
      <h1><a href="<%= request.getContextPath() %>/">PD-Dikti <small>Client</small></a></h1>
    </div>

    <%-- menu --%>
    <%@include file="/pages/menu.jsp" %>

    <%
    if (session.getAttribute("pesan") != null) {
        out.println("<span class=\"label label-success\" style=\"text-align: center;\">" + session.getAttribute("pesan") + "</span>");
        session.removeAttribute("pesan");
    }
    %>

    <%
    /** jika logout **/
    if (act.equals("Logout")) {
        session.invalidate();
        response.setHeader("Refresh", "2;URL=" + request.getContextPath() + "/");
    }
    %>

    <%-- dsp_matakuliah --%>
    <%@include file="/pages/matakuliah/dsp_matakuliah.jsp" %>

    <%-- js --%>
    <%@include file="/util/js.jsp" %>
  </body>
</html>
