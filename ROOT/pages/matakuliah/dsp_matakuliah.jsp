<%
String table = "mata_kuliah";
String filter = "";
String order = "";
String limit = "15";
String offset = "";

GetRecordset gr = new GetRecordset();
DefaultTableModel defaultTableModel = gr.getRecordset(token, table, endpoint, filter, order, limit, offset);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();
%>
<script type="text/javascript">
/* selectAll */
var checkAll = function() {
    if (document.getElementById("checkall").checked) {
        for (var i=0; i < 15; i++) {
            document.getElementById("check_" + i).checked = true;
        }
    }
    else {
        for (var i=0; i < 15; i++) {
            document.getElementById("check_" + i).checked = false;
        }
    }
};
</script>

<form method="post">
<table class="table table-bordered table-condensed">
    <thead>
        <th style="width:30px;text-align:center;">No</th>
        <th style="width:100px;text-align:center;">
            <input type="checkbox" id="checkall" onclick="checkAll();" title="Pilih semua!!!"/>
        </th>
        <th style="width:100px;text-align:center;">Kode MK</th>
        <th>Nama MK</th>
    </thead>
    <tbody>
        <%
        for (int i = 0; i < banyakData; i++) {
        %>
            <tr>
                <td><%= (i + 1) %></td>
                <td>
                    <input type="checkbox" id="check_<%= i %>" name="check_<%= i %>" value="<%= defaultTableModel.getValueAt(i, 0) %>" />
                </td>
                <td><%= defaultTableModel.getValueAt(i, 3) %></td>
                <td><%= defaultTableModel.getValueAt(i, 4) %></td>
            </tr>
        <%
        }
        %>
    </tbody>
</table>
</form>
