<%
String table = ( request.getParameter("table") == null ? "mahasiswa" : request.getParameter("table") );
%>
<h2>Tabel: <%= table %></h2>

<a href="<%= request.getContextPath() %>/pages/?act=ListTable" class="label label-success">Kembali</a>
<table class="table table-bordered table-endorsed">
    <tr>
        <th style="width:50px;">No</th>
<%
GetDictionary gd = new GetDictionary();
DefaultTableModel defaultTableModel = gd.getDictionary(token, table, endpoint);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();

    for (int i = 0; i < banyakKolom; i++) {
    %>
        <th><%= defaultTableModel.getColumnName(i) %></th>
    <%
    }
    %>
    </tr>

    <%-- iterasi data --%>
    <%
    for (int baris = 0; baris < banyakData; baris++) {
        out.println("<tr>");
            out.println("<td>" + (baris + 1) + "</td>");
        for (int kolom = 0; kolom < banyakKolom; kolom++) {
        %>
            <td><%= defaultTableModel.getValueAt(baris, kolom) %></td>
        <%
        }
        out.println("</tr>");
    }
    %>
</table>
