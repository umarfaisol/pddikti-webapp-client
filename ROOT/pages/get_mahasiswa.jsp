<%
String table = ( request.getParameter("table") == null ? "mahasiswa" : request.getParameter("table") );
String filter = ( request.getParameter("filter") == null ? "" : request.getParameter("filter") );
String order = ( request.getParameter("order") == null ? "" : request.getParameter("order") );
String limit = ( request.getParameter("limit") == null ? "100" : request.getParameter("limit") );
String offset = ( request.getParameter("offset") == null ? "" : request.getParameter("offset") );

table = "mahasiswa_pt";
%>

<%
if (session.getAttribute("pesan") != null) {
%>
    <span class="label label-success"><%= session.getAttribute("pesan") %></span><br/>
    <%
    session.removeAttribute("pesan");
}
%>

<form method="post" action="<%= request.getContextPath() %>/pages/">
    <input type="hidden" name="act" value="Mahasiswa" />
    <input type="hidden" name="table" value="<%= table %>" />
    <input type="hidden" name="order" value="<%= order %>" />
    <input type="hidden" name="offset" value="<%= offset %>" />

    <table>
        <tr>
            <td style="padding-right: 15px;">NIM / Nama</td>
            <td><input type="text" name="filter" size="50" value="<%= filter %>" /><input type="submit" value="Go" /></td>
        </tr>
        <tr>
            <td>Limit</td>
            <td><input type="text" name="limit" value="<%= limit %>" /></td>
        </tr>
    </table>
</form>

<a href="<%= request.getContextPath() %>/pages/" class="label label-success">Kembali</a>
<table class="table table-bordered table-endorsed">
    <tr>
        <th style="width:50px;">No</th>
<%
filter = "nipd LIKE '%" + filter + "%' OR LOWER(nm_pd) LIKE '%" + filter.toLowerCase() + "%'";
GetRecordset gr = new GetRecordset();
DefaultTableModel defaultTableModel = gr.getRecordset(token, table, endpoint, filter, order, limit, offset);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();
    %>
        <th style="width:130px;"><%= defaultTableModel.getColumnName(1) %></th>
        <th style="width:300px;"><%= defaultTableModel.getColumnName(3) %></th>
        <th><%= defaultTableModel.getColumnName(4) %></th>
        <th>Opsi</th>
    </tr>

    <%-- iterasi data --%>
    <%
    for (int baris = 0; baris < banyakData; baris++) {
        out.println("<tr>");
            out.println("<td>" + (baris + 1) + "</td>");
            String id = (String)defaultTableModel.getValueAt(baris, 2);
            %>
            <td><%= defaultTableModel.getValueAt(baris, 1) %></td>
            <td><%= defaultTableModel.getValueAt(baris, 3) %></td>
            <td><%= defaultTableModel.getValueAt(baris, 4) %></td>
            <td>
                <a class="btn btn-primary" href="<%= request.getContextPath() %>/pages/?act=Mahasiswa&proses=ubah&id_pd=<%= id %>"><span class="glyphicon glyphicon-edit"></span> Ubah</a>
            </td>
        <%
        out.println("</tr>");
    }
    %>
</table>
