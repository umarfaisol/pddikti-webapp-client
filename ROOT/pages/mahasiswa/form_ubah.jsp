<%
String id_pd = ( request.getParameter("id_pd") == null ? "" : request.getParameter("id_pd") );
String table = "mahasiswa";
String filter = "id_pd='" + id_pd + "'";
String order = "";
String limit = "";
String offset = "";

GetRecordset gr = new GetRecordset();
DefaultTableModel defaultTableModel = gr.getRecordset(token, table, endpoint, filter, order, limit, offset);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();

String nm_pd = (String)defaultTableModel.getValueAt(0, 1);
String jk = (String)defaultTableModel.getValueAt(0, 2);
String tmpt_lahir = (String)defaultTableModel.getValueAt(0, 5);
String tgl_lahir = (String)defaultTableModel.getValueAt(0, 6);
String rt = (String)defaultTableModel.getValueAt(0, 14);
String rw = (String)defaultTableModel.getValueAt(0, 15);
String jln = (String)defaultTableModel.getValueAt(0, 13);
String kode_pos = (String)defaultTableModel.getValueAt(0, 20);
String nm_ibu_kandung = (String)defaultTableModel.getValueAt(0, 42);

%>

<form action="<%= request.getContextPath() %>/pages/" method="post">
    <input type="hidden" name="id_pd" value="<%= id_pd %>" />
    <input type="hidden" name="act" value="Mahasiswa" />
    <input type="hidden" name="proses" value="simpan" />
    <table>
        <tr>
            <td style="padding-left:20px;"><label>Nama</label></td>
            <td style="padding-left:20px;"><input type="text" name="nm_pd" class="form-control" style="width:350px;" value="<%= nm_pd %>" /></td>
        </tr>
        <tr>
            <td style="padding-left:20px;"><label>Jenis Kelamin</label></td>
            <td style="padding-left:20px;"><input type="text" name="jk" class="form-control" style="width:60px;" value="<%= jk %>" /></td>
        </tr>
        <tr>
            <td style="padding-left:20px;"><label>Tempat Lahir</label></td>
            <td style="padding-left:20px;"><input type="text" name="tmpt_lahir" class="form-control" value="<%= tmpt_lahir %>" /></td>
        </tr>
        <tr>
            <td style="padding-left:20px;"><label>Tanggal Lahir</label></td>
            <td style="padding-left:20px;"><input type="text" name="tgl_lahir" class="form-control" value="<%= tgl_lahir %>" /></td>
        </tr>
        <%--
        <tr>
            <td style="padding-left:20px;"><label>RT</label></td>
            <td style="padding-left:20px;"><input type="text" name="rt" class="form-control" value="<%= rt %>" /></td>
        </tr>
        <tr>
            <td style="padding-left:20px;"><label>RW</label></td>
            <td style="padding-left:20px;"><input type="text" name="rw" class="form-control" value="<%= rw %>" /></td>
        </tr>
        <tr>
            <td style="padding-left:20px;"><label>Jalan</label></td>
            <td style="padding-left:20px;"><input type="text" name="jln" class="form-control" value="<%= jln %>" /></td>
        </tr>
        <tr>
            <td style="padding-left:20px;"><label>Kode Pos</label></td>
            <td style="padding-left:20px;"><input type="text" name="kode_pos" class="form-control" value="<%= kode_pos %>" /></td>
        </tr>--%>
        <tr>
            <td style="padding-left:20px;"><label>Nama Ibu Kandung</label></td>
            <td style="padding-left:20px;"><input type="text" name="nm_ibu_kandung" class="form-control" value="<%= nm_ibu_kandung %>" /></td>
        </tr>
        <tr>
            <td style="padding-top:20px;padding-left:20px;"><a href="<%= request.getContextPath() %>/pages/?act=Mahasiswa" class="btn btn-warning"><span class="glyphicon glyphicon-floppy-save"></span> Batal</a></td>
            <td style="padding-top:20px;padding-left:20px;">
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-save"></span> Simpan</button>
            </td>
        </tr>
    </table>
</form>

<%
/* sks_diakui
StringBuffer sb = new StringBuffer();
sb.append("{\"key\":");
    sb.append("{\"id_pd\":\"" + id_reg_pd + "\"},");
sb.append("\"data\":");
    sb.append("{\"sks_diakui\":0}");
sb.append("}");
*/

/* tgl_lahir *
StringBuffer sb = new StringBuffer();
sb.append("{\"key\":");
    sb.append("{\"id_pd\":\"" + id_smt + "\"},");
sb.append("\"data\":");
    sb.append("{\"tgl_lahir\":\"1992-01-01\"}");
sb.append("}");

/*
StringBuffer sb = new StringBuffer();
sb.append("{\"key\":");
    sb.append("{\"id_pd\":\"" + id_reg_pd + "\"},");
sb.append("\"data\":");
    sb.append("{\"tgl_lahir\":\"1991-04-12\",\"tmpt_lahir\":\"Selong Lombok\"}");
sb.append("}");
*/
%>
