<%@include file="/util/import.jsp" %>

<%
String token = ( session.getAttribute("token") == null ? "" : session.getAttribute("token").toString() );
String endpoint = ( session.getAttribute("endpoint") == null ? "" : session.getAttribute("endpoint").toString() );
String act = ( request.getParameter("act") == null ? "" : request.getParameter("act") );

if (token.equals("")) {
    response.sendRedirect(request.getContextPath() + "/");
}

/* set default endpoint */
if (endpoint.equals("")) {
    endpoint = "http://localhost:8082/ws/live.php";
}
%>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PD-Dikti Feeder Client | <%= token %></title>

        <%-- css --%>
        <%@include file="/util/css.jsp" %>
  </head>
  <body>
    <div class="page-header">
      <h1><a href="<%= request.getContextPath() %>/">PD-Dikti <small>Client</small></a></h1>
    </div>

    <%
    if (session.getAttribute("pesan") != null) {
        out.println("<span class=\"label label-success\" style=\"text-align: center;\">" + session.getAttribute("pesan") + "</span>");
        session.removeAttribute("pesan");
        session.removeAttribute("ke");
        session.removeAttribute("data");
    }
    %>

    <%
    /** jika logout **/
    if (act.equals("Logout")) {
        session.invalidate();
        response.setHeader("Refresh", "2;URL=" + request.getContextPath() + "/");
    }

    /* get recordset */
    else if (act.equals("GetRecordset")) { %>
        <%@include file="/pages/get_recordset.jsp" %>
    <%
    }

    /* unggah */
    /** -------------------------------------------------- **/
    else if (act.equals("Unggah")) {
        String proses = ( request.getParameter("proses") == null ? "" : request.getParameter("proses") );
        if (proses.equals("Preview")) { %>
            <%@include file="/pages/unggah/preview.jsp" %>
        <%
        }
        else { %>
            <%@include file="/pages/unggah/index.jsp" %>
        <%
        }
    }
    /** ---------------------------------------------------- **/

    /** kuliah mahasiswa **/
    else if (act.equals("Upload")) {
        String table = ( request.getParameter("table") == null ? "" : request.getParameter("table") );
        if (table.equals("kuliah_mahasiswa")) {
            response.sendRedirect(request.getContextPath() + "/pages/unggah/kuliah_mahasiswa/");
        }
        else if (table.equals("mahasiswa")) {
            response.sendRedirect(request.getContextPath() + "/pages/unggah/mahasiswa/");
        }
        else if (table.equals("lulusan")) {
            response.sendRedirect(request.getContextPath() + "/pages/unggah/lulusan/");
        }
        else if (table.equals("mahasiswa_pt")) {
            response.sendRedirect(request.getContextPath() + "/pages/unggah/mahasiswa_pt/");
        }
    }

    /** ---------------------------------------------------- **/

    /* Mahasiswa */
    else if (act.equals("Mahasiswa")) {
        String proses = ( request.getParameter("proses") == null ? "" : request.getParameter("proses") );
        if (proses.equals("ubah")) { %>
            <%@include file="/pages/mahasiswa/form_ubah.jsp" %>
        <%
        }
        else if (proses.equals("simpan")) { %>
            <%@include file="/pages/mahasiswa/simpan.jsp" %>
        <%
        }
        else { %>
            <%@include file="/pages/get_mahasiswa.jsp" %>
        <%
        }
    }

    /* get dictionary */
    else if (act.equals("GetDictionary")) { %>
        <%@include file="/pages/get_dictionary.jsp" %>
    <%
    }

    /** listtable **/
    else if (act.equals("ListTable")) { %>
        <%@include file="/pages/list_table.jsp" %>
    <%
    }
    /* menu */
    else { %>
        <%@include file="/pages/menu.jsp" %>
    <% } %>
    <%-- js --%>
    <%@include file="/util/js.jsp" %>
  </body>
</html>
