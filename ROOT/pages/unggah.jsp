<%
String table = ( request.getParameter("table") == null ? "mahasiswa" : request.getParameter("table") );
%>
<h2>Tabel: <%= table %></h2>
<form method="post" enctype="multipart/form-data">
    <input type="hidden" name="act" value="unggah" />
    <table class="table table-endorset">
        <tr>
            <td>File</td>
            <td>
                <input type="file" class="form-control" id="berkas" name="berkas" placeholder="Berkas Import" />
            </td>
        </tr>
    </table>
    <input type="submit" class="btn btn-primary" value="Unggah" />
</form>

<a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a>

<%--
<table class="table table-bordered table-endorsed">
    <tr>
        <th style="width:50px;">No</th>
<%
GetRecordset gr = new GetRecordset();
DefaultTableModel defaultTableModel = gr.getRecordset(token, table, endpoint, filter, order, limit, offset);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();

    for (int i = 0; i < banyakKolom; i++) {
    %>
        <th><%= defaultTableModel.getColumnName(i) %></th>
    <%
    }
    %>
    </tr>

    <%-- iterasi data --%
    <%
    for (int baris = 0; baris < banyakData; baris++) {
        out.println("<tr>");
            out.println("<td>" + (baris + 1) + "</td>");
        for (int kolom = 0; kolom < banyakKolom; kolom++) {
        %>
            <td><%= defaultTableModel.getValueAt(baris, kolom) %></td>
        <%
        }
        out.println("</tr>");
    }
    %>
</table>
--%>
