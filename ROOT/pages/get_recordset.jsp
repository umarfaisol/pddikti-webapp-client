<%
String table = ( request.getParameter("table") == null ? "mahasiswa" : request.getParameter("table") );
String filter = ( request.getParameter("filter") == null ? "" : request.getParameter("filter") );
String order = ( request.getParameter("order") == null ? "" : request.getParameter("order") );
String limit = ( request.getParameter("limit") == null ? "100" : request.getParameter("limit") );
String offset = ( request.getParameter("offset") == null ? "" : request.getParameter("offset") );
%>
<h2>Tabel: <%= table %></h2>
<form method="post">
    <input type="hidden" name="act" value="GetRecordset" />
    <table class="table table-endorset">
        <tr>
            <td>Table</td>
            <td><input type="text" name="table" size="30" value="<%= table %>"></td>
        </tr>
        <tr>
            <td>Filter</td>
            <td><input type="text" name="filter" size="70" value="<%= filter %>"></td>
        </tr>
        <tr>
            <td>Order</td>
            <td><input type="text" name="order" size="30" value="<%= order %>"></td>
        </tr>
        <tr>
            <td>Limit</td>
            <td><input type="text" name="limit" value="<%= limit %>"></td>
        </tr>
        <tr>
            <td>Offset</td>
            <td><input type="text" name="offset" value="<%= offset %>"></td>
        </tr>
    </table>
    <input type="submit" value="Go" />
</form>

<a href="<%= request.getContextPath() %>/pages/?act=ListTable" class="label label-success">Kembali</a>
<table class="table table-bordered table-endorsed">
    <tr>
        <th style="width:50px;">No</th>
<%
GetRecordset gr = new GetRecordset();
DefaultTableModel defaultTableModel = gr.getRecordset(token, table, endpoint, filter, order, limit, offset);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();

    for (int i = 0; i < banyakKolom; i++) {
    %>
        <th><%= defaultTableModel.getColumnName(i) %></th>
    <%
    }
    %>
    </tr>

    <%-- iterasi data --%>
    <%
    for (int baris = 0; baris < banyakData; baris++) {
        out.println("<tr>");
            out.println("<td>" + (baris + 1) + "</td>");
        for (int kolom = 0; kolom < banyakKolom; kolom++) {
        %>
            <td><%= defaultTableModel.getValueAt(baris, kolom) %></td>
        <%
        }
        out.println("</tr>");
    }
    %>
</table>
