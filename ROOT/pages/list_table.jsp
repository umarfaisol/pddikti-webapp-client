<h2>Daftar Tabel</h2>
<a href="<%= request.getContextPath() %>/pages/" class="label label-success">Kembali</a>
<table class="table table-bordered table-endorsed">
    <tr>
        <th>No</th>
<%
ListTable lt = new ListTable();
DefaultTableModel defaultTableModel = lt.listTable(token, endpoint);
int banyakData = defaultTableModel.getRowCount();
int banyakKolom = defaultTableModel.getColumnCount();

    for (int i = 0; i < banyakKolom; i++) {
    %>
        <th><%= defaultTableModel.getColumnName(i) %></th>
    <%
    }
    %>
        <th>Opsi</th>
    </tr>

    <%-- iterasi data --%>
    <%
    for (int baris = 0; baris < banyakData; baris++) {
        out.println("<tr>");
            out.println("<td>" + (baris + 1) + "</td>");
        for (int kolom = 0; kolom < banyakKolom; kolom++) {
        %>
            <td><%= defaultTableModel.getValueAt(baris, kolom) %></td>
        <%
        }
        %>
            <td>
                <a href="<%= request.getContextPath() %>/pages/?act=GetRecordset&table=<%= defaultTableModel.getValueAt(baris, 0) %>" class="label label-primary">Data</a>
                <a href="<%= request.getContextPath() %>/pages/?act=GetDictionary&table=<%= defaultTableModel.getValueAt(baris, 0) %>" class="label label-primary">Struktur</a>
            </td>
        <%
        out.println("</tr>");
    }
    %>
</table>
