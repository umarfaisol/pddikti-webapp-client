<%
String table = ( request.getParameter("table") == null ? "mahasiswa" : request.getParameter("table") );
String statusUpload = ( request.getParameter("statusUpload") == null ? "" : request.getParameter("statusUpload") );

if (statusUpload.equals("upload")) { %>
    <%
    ArrayList data = (ArrayList)session.getAttribute("data");
    String banyakData = ( session.getAttribute("banyakData") == null ? "0" : session.getAttribute("banyakData").toString() );
    String ke = ( session.getAttribute("ke") == null ? "1" : (String)session.getAttribute("ke") );
    %>
    <h3>Sedang mengunggah... [<%= table %>]</h3>
    <span class="label label-success" style="font-size:20px;">Data ke: <b><%= ke %></b> dari <b><%= banyakData %></b></span><br/><br/>
    <a href="<%= request.getContextPath() %>/" class="label label-primary">Kembali</a><br/><br/>
    <table class="table table-bordered table-endorsed">
        <tr>
            <th>No</th>

    <%
    String isi[] = (String[])data.get(0);/*
    for (int i = 0; i < isi.length; i++) {
        out.println(isi[i] + " --> ");
    }*/
    InsertRecord ir = new InsertRecord();
    GetRecordset gr = new GetRecordset();
    DefaultTableModel sms = gr.getRecordset(token, "sms", endpoint, "kode_prodi='" + isi[0] + "'", "", "1", "");
    String id_sms = "";
    if (sms.getRowCount() > 0) {
        id_sms = (String)sms.getValueAt(0, 0);
    }
    out.println("id_sms: " + id_sms + "<br/>");

    Map obj = new LinkedHashMap();
    obj.put("id_sms", id_sms);
    obj.put("nilai_huruf", isi[1]);
    obj.put("bobot_nilai_min", isi[2]);
    obj.put("bobot_nilai_maks", isi[3]);
    obj.put("nilai_indeks", isi[4]);
    obj.put("tgl_mulai_efektif", isi[5]);
    obj.put("tgl_akhir_efektif", isi[6]);
    String jsonText = JSONValue.toJSONString(obj);
    out.println("InsertRecord() --><br/>" + jsonText + "<br/>");

    /* data bobot *
    StringBuffer sb = new StringBuffer();
    sb.append("{");
        sb.append("\"id_sms\":\"" + isi[0] + "\",");
        sb.append("\"nilai_huruf\":\"" + isi[1] + "\",");
        sb.append("\"bobot_nilai_min\":\"" + isi[2] + "\",");
        sb.append("\"bobot_nilai_maks\":\"" + isi[3] + "\",");
        sb.append("\"nilai_indeks\":\"" + isi[4] + "\",");
        sb.append("\"tgl_mulai_efektif\":\"" + isi[5] + "\",");
        sb.append("\"tgl_akhir_efektif\":\"" + isi[6] + "\"");
    sb.append("}");
    out.println(sb.toString());*/

    DefaultTableModel defaultTableModel = ir.insertRecord(token, table, endpoint, jsonText);
    int banyak = defaultTableModel.getRowCount();
    int banyakKolom = defaultTableModel.getColumnCount();

        for (int i = 0; i < banyakKolom; i++) {
        %>
            <th><%= defaultTableModel.getColumnName(i) %></th>
        <%
        }
        %>
            <th>Opsi</th>
        </tr>

        <%-- iterasi data --%>
        <%
        for (int baris = 0; baris < banyak; baris++) {
            out.println("<tr>");
                out.println("<td>" + (baris + 1) + "</td>");
            for (int kolom = 0; kolom < banyakKolom; kolom++) {
            %>
                <td><%= defaultTableModel.getValueAt(baris, kolom) %></td>
            <%
            }
            %>
                <td>
                </td>
            <%
            out.println("</tr>");
        }
        data.remove(0);
        data.trimToSize();
        session.setAttribute("data", data);

        ke = new Integer(Integer.valueOf(ke).intValue() + 1).toString();
        session.setAttribute("ke", ke);

        if (Integer.valueOf(ke).intValue() <= Integer.valueOf(banyakData).intValue()) {
            out.println("<br/><br/><span class=\"label label-success\">Ke data selanjutnya...</span>");
            response.setHeader("Refresh", "1;URL=" + request.getContextPath() + "/pages/?act=Unggah&statusUpload=upload&table=" + table);
        }
        else {
            session.setAttribute("pesan", "Data telah diupload ke tabel " + table);
            response.setHeader("Refresh", "5;URL=" + request.getContextPath() + "/pages/");
        }
        %>
    </table>
<%
}
else {
%>
    <h2>Tabel: <%= table %></h2>
    <a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a>
    <form action="<%= request.getContextPath() %>/pages/?act=Unggah&proses=Preview&table=<%= table %>" method="post" enctype="multipart/form-data">
        <%--<input type="hidden" name="act" value="Unggah" />
        <input type="hidden" name="proses" value="Preview" />
        <input type="hidden" name="table" value="<%= table %>" />--%>

        <input type="file" name="berkas" />
        <input type="submit" class="btn btn-primary" value="Unggah" />
    </form>
<%
}
%>
