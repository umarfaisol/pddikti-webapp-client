<%@page session="true" %>

<%
String table = "mahasiswa";
%>
<h2>Tabel: <%= table %></h2>
<a href="<%= request.getContextPath() %>/pages/unggah/mahasiswa/" class="label label-success">Kembali</a><br/>

<%
//process only if its multipart content
if(ServletFileUpload.isMultipartContent(request)) {
    Workbook wb = null;

    try {
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for (FileItem item : multiparts) {
            if (!item.isFormField()) {
                String name = item.getName();
                out.println("File: " + name);
                String type = "";
                if (name.toLowerCase().endsWith("xls")) {
                    type = "xls";/*
                    NPOIFSFileSytem fs = new NPOIFSFileSystem(item.getInputStream());
                    wb = new HSSFWorkbook(fs.getRoot());*/
                }
                else if (name.toLowerCase().endsWith("xlsx")) {
                    type = "xlsx";/*
                    OPCPackage pkg = OPCPackage.open(item.getInputStream());
                    wb = new XSSFWorkbook(pkg);*/
                }
                else {
                    type = "no";
                }
                out.println("<br/>Type: " + type);
                out.println("<br/><span class=\"label label-success\">Preview 10 Data</span>");
                out.println("<table class=\"table table-bordered table-condensed\">");

                /* proses */
                ArrayList listData = new ArrayList();
                if (!type.equals("no")) {
                    wb = WorkbookFactory.create(item.getInputStream());
                    Sheet sheet = wb.getSheetAt(0);

                    int i = 0;
                    for (Row row : sheet) {
                        i++;

                        String nm_pd = ( row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue());
                        String jk = ( row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue());
                        String nik = ( row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue());
                        String tmpt_lahir = ( row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue());
                        String tgl_lahir = ( row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue());
                        String id_agama = ( row.getCell(5) == null ? "" : row.getCell(5).getStringCellValue());
                        String id_kk = ( row.getCell(6) == null ? "" : row.getCell(6).getStringCellValue());
                        String jln = ( row.getCell(7) == null ? "" : row.getCell(7).getStringCellValue());
                        String rt = ( row.getCell(8) == null ? "" : row.getCell(8).getStringCellValue());
                        String rw = ( row.getCell(9) == null ? "" : row.getCell(9).getStringCellValue());
                        String nm_dsn = ( row.getCell(10) == null ? "" : row.getCell(10).getStringCellValue());
                        String ds_kel = ( row.getCell(11) == null ? "" : row.getCell(11).getStringCellValue());
                        String id_wil = ( row.getCell(12) == null ? "" : row.getCell(12).getStringCellValue());
                        String kode_pos = ( row.getCell(13) == null ? "" : row.getCell(13).getStringCellValue());
                        String a_terima_kps = ( row.getCell(14) == null ? "" : row.getCell(14).getStringCellValue());
                        String nm_ayah = ( row.getCell(15) == null ? "" : row.getCell(15).getStringCellValue());
                        String id_kebutuhan_khusus_ayah = ( row.getCell(16) == null ? "" : row.getCell(16).getStringCellValue());
                        String nm_ibu_kandung = ( row.getCell(17) == null ? "" : row.getCell(17).getStringCellValue());
                        String id_kebutuhan_khusus_ibu = ( row.getCell(18) == null ? "" : row.getCell(18).getStringCellValue());
                        String kewarganegaraan = ( row.getCell(19) == null ? "" : row.getCell(19).getStringCellValue());

                        String data[] = new String[20];
                        data[0] = nm_pd;
                        data[1] = jk;
                        data[2] = nik;
                        data[3] = tmpt_lahir;
                        data[4] = tgl_lahir;
                        data[5] = id_agama;
                        data[6] = id_kk;
                        data[7] = jln;
                        data[8] = rt;
                        data[9] = rw;
                        data[10] = nm_dsn;
                        data[11] = ds_kel;
                        data[12] = id_wil;
                        data[13] = kode_pos;
                        data[14] = a_terima_kps;
                        data[15] = nm_ayah;
                        data[16] = id_kebutuhan_khusus_ayah;
                        data[17] = nm_ibu_kandung;
                        data[18] = id_kebutuhan_khusus_ibu;
                        data[19] = kewarganegaraan;

                        listData.add(data);
                    }
                }

                /** tes data **/
                listData.remove(0);
                listData.trimToSize();
                session.setAttribute("data", listData);
                session.setAttribute("banyakData", listData.size());

                for (int i = 0; i < listData.size(); i++) {
                    if (i <= 10) {
                        out.println("<tr>");
                        String data[] = (String[])listData.get(i);
                        for (int k = 0; k < data.length; k++) {
                            out.println("<td>" + data[k] + "</td>");
                        }
                        out.println("</tr>");
                    }
                }


                out.println("<tr><td><b>" + new Integer(listData.size()).toString() + "</b> Data " + table + "</td><td>");
                    out.println("<form action=\"" + request.getContextPath() + "/pages/unggah/mahasiswa/?statusUpload=upload\" method=\"post\">");
                        out.println("<button type=\"submit\" class=\"btn btn-primary\">Proses</button>");
                    out.println("</form>");
                out.println("</td></tr>");
                out.println("</table>");
            }
        }

        //File uploaded successfully
        out.println("<br/>File Uploaded Successfully");
    }
    catch (Exception ex) {
        out.println("File Upload Failed due to " + ex);
    }

}
else {
    out.println("--- ---");
}
%>
