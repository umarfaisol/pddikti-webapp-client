<%
String table = "mahasiswa";
String statusUpload = ( request.getParameter("statusUpload") == null ? "" : request.getParameter("statusUpload") );

if (statusUpload.equals("upload")) { %>
    <%
    ArrayList data = (ArrayList)session.getAttribute("data");
    String banyakData = ( session.getAttribute("banyakData") == null ? "0" : session.getAttribute("banyakData").toString() );
    String ke = ( session.getAttribute("ke") == null ? "1" : (String)session.getAttribute("ke") );
    %>
    <h3>Sedang mengunggah... [<%= table %>]</h3>
    <span class="label label-success" style="font-size:20px;">Data ke: <b><%= ke %></b> dari <b><%= banyakData %></b></span><br/><br/>
    <a href="<%= request.getContextPath() %>/" class="label label-primary">Kembali</a><br/><br/>
    <table class="table table-bordered table-endorsed">
        <tr>
            <th>No</th>

    <%
        InsertRecord ir = new InsertRecord();
        GetRecordset gr = new GetRecordset();
        UpdateRecord ur = new UpdateRecord();

        String isi[] = (String[])data.get(0);

        /* cek */
        String nm_pd = isi[0];
        String jk = isi[1];
        String tgl_lahir = isi[4];

        DefaultTableModel mhs = gr.getRecordset(token, "mahasiswa", endpoint, "LOWER(nm_pd) LIKE '%" + nm_pd.toLowerCase() + "%' AND jk='" + jk + "' AND tgl_lahir='" + tgl_lahir + "'", "", "1", "");
        if (mhs.getRowCount() > 0) {
            out.println("Mahasiswa sudah ada... !!!<br/>");
            out.println("Data dilewati... !!!<br/>");
        }
        else {
            out.println("Mahasiswa belum ada!!!<br/>");
            out.println("INSERT!!!<br/>");
            DefaultTableModel sp = gr.getRecordset(token, "satuan_pendidikan", endpoint, "npsn='" + isi[8] + "'", "", "1", "");
            String id_sp = "";
            if (sp.getRowCount() > 0) {
                id_sp = (String)sp.getValueAt(0, 0);
            }

            Map obj = new LinkedHashMap();
            obj.put("nm_pd", isi[0]);
            obj.put("jk", isi[1]);
            obj.put("nik", isi[2]);
            obj.put("tmpt_lahir", isi[3]);
            obj.put("tgl_lahir", isi[4]);
            obj.put("id_agama", isi[5]);
            obj.put("id_kk", isi[6]);
            obj.put("jln", isi[7]);
            obj.put("rt", isi[8]);
            obj.put("rw", isi[9]);
            obj.put("nm_dsn", isi[10]);
            obj.put("ds_kel", isi[11]);
            obj.put("id_wil", isi[12]);
            obj.put("kode_pos", isi[13]);
            obj.put("a_terima_kps", isi[14]);
            obj.put("nm_ayah", isi[15]);
            obj.put("id_kebutuhan_khusus_ayah", isi[16]);
            obj.put("nm_ibu_kandung", isi[17]);
            obj.put("id_kebutuhan_khusus_ibu", isi[18]);
            obj.put("kewarganegaraan", isi[19]);

            String jsonText = JSONValue.toJSONString(obj);
            out.println("InsertRecord() -->\n" + jsonText + "<br/>");

            DefaultTableModel defaultTableModel = ir.insertRecord(token, "mahasiswa", endpoint, jsonText);
            int banyak = defaultTableModel.getRowCount();
            int banyakKolom = defaultTableModel.getColumnCount();
        }

        /*

        out.println("Insert....<br/>");
        Map obj = new LinkedHashMap();
        obj.put("id_smt", isi[0]);
        obj.put("id_reg_pd", id_reg_pd);
        obj.put("ips", isi[2]);
        obj.put("sks_smt", isi[3]);
        obj.put("ipk", isi[4]);
        obj.put("sks_total", isi[5]);
        obj.put("id_stat_mhs", isi[6]);
        String jsonText = JSONValue.toJSONString(obj);
        out.println("InsertRecord() -->\n" + jsonText + "<br/>");

        DefaultTableModel defaultTableModel = ir.insertRecord(token, "kuliah_mahasiswa", endpoint, jsonText);
        int banyak = defaultTableModel.getRowCount();
        int banyakKolom = defaultTableModel.getColumnCount();
        */
        data.remove(0);
        data.trimToSize();
        session.setAttribute("data", data);

        ke = new Integer(Integer.valueOf(ke).intValue() + 1).toString();
        session.setAttribute("ke", ke);

        if (Integer.valueOf(ke).intValue() <= Integer.valueOf(banyakData).intValue()) {
            out.println("<br/><br/><span class=\"label label-success\">Ke data selanjutnya...</span>");
            response.setHeader("Refresh", "1;URL=" + request.getContextPath() + "/pages/unggah/mahasiswa/?statusUpload=upload");
        }
        else {
            session.setAttribute("pesan", "Data telah diupload ke tabel " + table);
            response.setHeader("Refresh", "25;URL=" + request.getContextPath() + "/pages/unggah/mahasiswa/");
        }
        %>
    </table>
<%
}
else {
%>
    <h2>Tabel: <%= table %></h2>
    <a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a>
    <form action="<%= request.getContextPath() %>/pages/unggah/mahasiswa/?proses=preview" method="post" enctype="multipart/form-data">
        <input type="file" name="berkas" />
        <input type="submit" class="btn btn-primary" value="Unggah" />
    </form>
<%
}
%>
