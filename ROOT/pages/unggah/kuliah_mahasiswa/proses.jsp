<%
String table = "kuliah_mahasiswa";
String statusUpload = ( request.getParameter("statusUpload") == null ? "" : request.getParameter("statusUpload") );

if (statusUpload.equals("upload")) { %>
    <%
    ArrayList data = (ArrayList)session.getAttribute("data");
    String banyakData = ( session.getAttribute("banyakData") == null ? "0" : session.getAttribute("banyakData").toString() );
    String ke = ( session.getAttribute("ke") == null ? "1" : (String)session.getAttribute("ke") );
    %>
    <h3>Sedang mengunggah... [<%= table %>]</h3>
    <span class="label label-success" style="font-size:20px;">Data ke: <b><%= ke %></b> dari <b><%= banyakData %></b></span><br/><br/>

    <%-- tambahi progressbar --%>
    <%
    int intKe = new Integer(ke).intValue();
    int intData = new Integer(banyakData).intValue();
    int persen = (int)((intKe * 100) / intData);
    %>

    <%--<%= persen %>--%>
    <div class="progress">
        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="<%= persen %>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <%= persen %>%;">
            <%= persen %> % selesai
        </div>
    </div>
    <%-- akhir progress --%>

    <a href="<%= request.getContextPath() %>/" class="label label-primary">Kembali</a><br/><br/>

    <%
        InsertRecord ir = new InsertRecord();
        GetRecordset gr = new GetRecordset();
        UpdateRecord ur = new UpdateRecord();

        String isi[] = (String[])data.get(0);
        String smt = isi[0];
        DefaultTableModel reg_pd = gr.getRecordset(token, "mahasiswa_pt", endpoint, "TRIM(nipd)='" + isi[1] + "'", "", "1", "");
        String id_reg_pd = "";
        if (reg_pd.getRowCount() > 0) {
            id_reg_pd = (String)reg_pd.getValueAt(0, 0);
        }
        out.println("id_reg_pd: " + id_reg_pd + "<br/>");

        /** cek ada akm apa tidak **/
        DefaultTableModel akm = gr.getRecordset(token, "kuliah_mahasiswa", endpoint, "id_reg_pd='" + id_reg_pd + "' AND id_smt='" + smt + "'", "", "1", "");
        /* jika ada */
        if (akm.getRowCount() > 0) {
            out.println("ADA -- update<br/>");
            Map objKey = new LinkedHashMap();
            objKey.put("id_reg_pd", id_reg_pd);
            objKey.put("id_smt", smt);

            Map objData = new LinkedHashMap();
            objData.put("ips", isi[2]);
            objData.put("sks_smt", isi[3]);
            objData.put("ipk", isi[4]);
            objData.put("sks_total", isi[5]);
            objData.put("id_stat_mhs", isi[6]);

            Map obj = new LinkedHashMap();
            obj.put("key", objKey);
            obj.put("data", objData);
            String jsonText = JSONValue.toJSONString(obj);
            out.println("UpdateRecord() -->\n" + jsonText + "<br/>");

            DefaultTableModel defaultTableModel = ur.updateRecord(token, "kuliah_mahasiswa", endpoint, jsonText);
            int banyak = defaultTableModel.getRowCount();
            int banyakKolom = defaultTableModel.getColumnCount();
        }

        /* tak ada */
        else {
            if (!id_reg_pd.equals("")) {
                out.println("Insert....<br/>");
                Map obj = new LinkedHashMap();
                obj.put("id_smt", isi[0]);
                obj.put("id_reg_pd", id_reg_pd);
                obj.put("ips", isi[2]);
                obj.put("sks_smt", isi[3]);
                obj.put("ipk", isi[4]);
                obj.put("sks_total", isi[5]);
                obj.put("id_stat_mhs", isi[6]);
                String jsonText = JSONValue.toJSONString(obj);
                out.println("InsertRecord() -->\n" + jsonText + "<br/>");

                DefaultTableModel defaultTableModel = ir.insertRecord(token, "kuliah_mahasiswa", endpoint, jsonText);
                int banyak = defaultTableModel.getRowCount();
                int banyakKolom = defaultTableModel.getColumnCount();
            }
        }

        /** jika dia lulus, maka update status mahasiswa **/
        if (isi[6].equals("L")) {
            DefaultTableModel pd = gr.getRecordset(token, "mahasiswa_pt", endpoint, "TRIM(nipd)='" + isi[1] + "'", "", "1", "");
            String id_pd = "";
            if (reg_pd.getRowCount() > 0) {
                id_pd = (String)reg_pd.getValueAt(0, 2);
            }
            out.println("id_reg_pd: " + id_reg_pd + "<br/>");
            out.println("id_pd: " + id_pd + "<br/>");

            /* update status mahasiswa */
            Map objKey = new LinkedHashMap();
            objKey.put("id_pd", id_pd);

            Map objData = new LinkedHashMap();
            objData.put("stat_pd", isi[6]);

            Map obj = new LinkedHashMap();
            obj.put("key", objKey);
            obj.put("data", objData);
            String jsonText = JSONValue.toJSONString(obj);
            out.println("Update status...<br/>UpdateRecord() --><br/>" + jsonText + "<br/>");

            DefaultTableModel defaultTableModel = ur.updateRecord(token, "mahasiswa", endpoint, jsonText);

            /* update status reg_pd */
            objKey = new LinkedHashMap();
            objKey.put("id_reg_pd", id_reg_pd);

            objData = new LinkedHashMap();
            objData.put("id_jns_keluar", "1");

            obj = new LinkedHashMap();
            obj.put("key", objKey);
            obj.put("data", objData);
            jsonText = JSONValue.toJSONString(obj);
            out.println("Update reg_pd...<br/>UpdateRecord() --><br/>" + jsonText + "<br/>");

            defaultTableModel = ur.updateRecord(token, "mahasiswa_pt", endpoint, jsonText);

            int banyak = defaultTableModel.getRowCount();
            int banyakKolom = defaultTableModel.getColumnCount();
        }

        data.remove(0);
        data.trimToSize();
        session.setAttribute("data", data);

        ke = new Integer(Integer.valueOf(ke).intValue() + 1).toString();
        session.setAttribute("ke", ke);

        if (Integer.valueOf(ke).intValue() <= Integer.valueOf(banyakData).intValue()) {
            out.println("<br/><br/><span class=\"label label-success\">Ke data selanjutnya...</span>");
            response.setHeader("Refresh", "1;URL=" + request.getContextPath() + "/pages/unggah/kuliah_mahasiswa/?statusUpload=upload");
        }
        else {
            session.setAttribute("pesan", "Data telah diupload ke tabel " + table);
            response.setHeader("Refresh", "5;URL=" + request.getContextPath() + "/");
        }
        %>
<%
}
else {
%>
    <h2>Tabel: <%= table %></h2>
    <a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a>
    <form action="<%= request.getContextPath() %>/pages/unggah/kuliah_mahasiswa/?proses=preview" method="post" enctype="multipart/form-data">
        <%--<input type="hidden" name="act" value="Unggah" />
        <input type="hidden" name="proses" value="Preview" />
        <input type="hidden" name="table" value="<%= table %>" />--%>

        <input type="file" name="berkas" />
        <input type="submit" class="btn btn-primary" value="Unggah" />
    </form>
<%
}
%>
