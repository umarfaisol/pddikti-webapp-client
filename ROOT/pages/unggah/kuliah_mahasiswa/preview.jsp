<%@page session="true" %>

<%
String table = "kuliah_mahasiswa";
%>
<h2>Tabel: <%= table %></h2>
<a href="<%= request.getContextPath() %>/pages/unggah/kuliah_mahasiswa/" class="label label-success">Kembali</a><br/>

<%
//process only if its multipart content
if(ServletFileUpload.isMultipartContent(request)) {
    Workbook wb = null;

    try {
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for (FileItem item : multiparts) {
            if (!item.isFormField()) {
                String name = item.getName();
                out.println("File: " + name);
                String type = "";
                if (name.toLowerCase().endsWith("xls")) {
                    type = "xls";/*
                    NPOIFSFileSytem fs = new NPOIFSFileSystem(item.getInputStream());
                    wb = new HSSFWorkbook(fs.getRoot());*/
                }
                else if (name.toLowerCase().endsWith("xlsx")) {
                    type = "xlsx";/*
                    OPCPackage pkg = OPCPackage.open(item.getInputStream());
                    wb = new XSSFWorkbook(pkg);*/
                }
                else {
                    type = "no";
                }
                out.println("<br/>Type: " + type);
                out.println("<br/><span class=\"label label-success\">Preview 10 Data</span>");
                out.println("<table class=\"table table-bordered table-condensed\">");

                /* proses */
                ArrayList listData = new ArrayList();
                if (!type.equals("no")) {
                    wb = WorkbookFactory.create(item.getInputStream());
                    Sheet sheet = wb.getSheetAt(0);

                    int i = 0;
                    for (Row row : sheet) {
                        i++;
                        /*
                        for (Cell cell : row) {
                            sb.append(cell.getRichStringCellValue().getString());
                        }
                        sb.append("<br/>");
                        */
                        String id_smt = ( row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue());
                        String nim = ( row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue());
                        String ips = ( row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue());
                        String sks_smt = ( row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue());
                        String ipk = ( row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue());
                        String sks_total = ( row.getCell(5) == null ? "" : row.getCell(5).getStringCellValue());
                        String id_stat_mhs = ( row.getCell(6) == null ? "" : row.getCell(6).getStringCellValue());

                        String data[] = new String[7];
                        data[0] = id_smt;
                        data[1] = nim;
                        data[2] = ips;
                        data[3] = sks_smt;
                        data[4] = ipk;
                        data[5] = sks_total;
                        data[6] = id_stat_mhs;

                        listData.add(data);
                    }
                }

                /** tes data **/
                listData.remove(0);
                listData.trimToSize();
                session.setAttribute("data", listData);
                session.setAttribute("banyakData", listData.size());

                for (int i = 0; i < listData.size(); i++) {
                    if (i <= 10) {
                        out.println("<tr>");
                        String data[] = (String[])listData.get(i);
                        for (int k = 0; k < data.length; k++) {
                            out.println("<td>" + data[k] + "</td>");
                        }
                        out.println("</tr>");
                    }
                }


                out.println("<tr><td><b>" + new Integer(listData.size()).toString() + "</b> Data " + table + "</td><td>");
                    out.println("<form action=\"" + request.getContextPath() + "/pages/unggah/kuliah_mahasiswa/?statusUpload=upload\" method=\"post\">");
                        /*out.println("<input type=\"hidden\" name=\"act\" value=\"Unggah\" />");
                        out.println("<input type=\"hidden\" name=\"proses\" value=\"Preview\" />");
                        out.println("<input type=\"hidden\" name=\"statusUpload\" value=\"upload\" />");
                        out.println("<input type=\"hidden\" name=\"table\" value=\"" + table + "\" />");*/
                        out.println("<button type=\"submit\" class=\"btn btn-primary\">Proses</button>");
                    out.println("</form>");
                out.println("</td></tr>");
                out.println("</table>");
            }
        }

        //File uploaded successfully
        out.println("<br/>File Uploaded Successfully");
    }
    catch (Exception ex) {
        out.println("File Upload Failed due to " + ex);
    }

}
else {
    out.println("--- ---");
}
%>
