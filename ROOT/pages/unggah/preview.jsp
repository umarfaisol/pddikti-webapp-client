<%@page session="true" %>

<%
String table = ( request.getParameter("table") == null ? "mahasiswa" : request.getParameter("table") );
%>
<h2>Tabel: <%= table %></h2>
<a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a><br/>

<%
//process only if its multipart content
if(ServletFileUpload.isMultipartContent(request)) {
    Workbook wb = null;

    try {
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for (FileItem item : multiparts) {
            if (!item.isFormField()) {
                String name = item.getName();
                out.println("File: " + name);
                String type = "";
                if (name.toLowerCase().endsWith("xls")) {
                    type = "xls";/*
                    NPOIFSFileSytem fs = new NPOIFSFileSystem(item.getInputStream());
                    wb = new HSSFWorkbook(fs.getRoot());*/
                }
                else if (name.toLowerCase().endsWith("xlsx")) {
                    type = "xlsx";/*
                    OPCPackage pkg = OPCPackage.open(item.getInputStream());
                    wb = new XSSFWorkbook(pkg);*/
                }
                else {
                    type = "no";
                }
                out.println("<br/>Type: " + type);
                out.println("<br/><span class=\"label label-success\">Preview 10 Data</span>");
                out.println("<table class=\"table table-bordered table-condensed\">");

                /* proses */
                ArrayList listData = new ArrayList();
                if (!type.equals("no")) {
                    wb = WorkbookFactory.create(item.getInputStream());
                    Sheet sheet = wb.getSheetAt(0);

                    int i = 0;
                    for (Row row : sheet) {
                        i++;
                        /*
                        for (Cell cell : row) {
                            sb.append(cell.getRichStringCellValue().getString());
                        }
                        sb.append("<br/>");
                        */
                        String id_sms = ( row.getCell(1) == null ? "" : row.getCell(1).getRichStringCellValue().getString());
                        String nilai_huruf = ( row.getCell(2) == null ? "" : row.getCell(2).getRichStringCellValue().getString());
                        String bobot_nilai_min = ( row.getCell(3) == null ? "" : row.getCell(3).getRichStringCellValue().getString());
                        String bobot_nilai_maks = ( row.getCell(4) == null ? "" : row.getCell(4).getRichStringCellValue().getString());
                        String nilai_indeks = ( row.getCell(5) == null ? "" : row.getCell(5).getRichStringCellValue().getString());
                        String tgl_mulai_efektif = ( row.getCell(6) == null ? "" : row.getCell(6).getRichStringCellValue().getString());
                        String tgl_akhir_efektif = ( row.getCell(7) == null ? "" : row.getCell(7).getRichStringCellValue().getString());

                        String data[] = new String[7];
                        data[0] = id_sms;
                        data[1] = nilai_huruf;
                        data[2] = bobot_nilai_min;
                        data[3] = bobot_nilai_maks;
                        data[4] = nilai_indeks;
                        data[5] = tgl_mulai_efektif;
                        data[6] = tgl_akhir_efektif;
                        listData.add(data);
                    }
                }

                /** tes data **/
                listData.remove(0);
                listData.trimToSize();
                session.setAttribute("data", listData);
                session.setAttribute("banyakData", listData.size());

                for (int i = 0; i < listData.size(); i++) {
                    if (i <= 10) {
                        out.println("<tr>");
                        String data[] = (String[])listData.get(i);
                        for (int k = 0; k < data.length; k++) {
                            out.println("<td>" + data[k] + "</td>");
                        }
                        out.println("</tr>");
                    }
                }


                out.println("<tr><td><b>" + new Integer(listData.size()).toString() + "</b> Data " + table + "</td><td>");
                    out.println("<form action=\"" + request.getContextPath() + "/pages/?act=Unggah&statusUpload=upload&table=" + table + "\" method=\"post\">");
                        /*out.println("<input type=\"hidden\" name=\"act\" value=\"Unggah\" />");
                        out.println("<input type=\"hidden\" name=\"proses\" value=\"Preview\" />");
                        out.println("<input type=\"hidden\" name=\"statusUpload\" value=\"upload\" />");
                        out.println("<input type=\"hidden\" name=\"table\" value=\"" + table + "\" />");*/
                        out.println("<button type=\"submit\" class=\"btn btn-primary\">Proses</button>");
                    out.println("</form>");
                out.println("</td></tr>");
                out.println("</table>");
            }
        }

        //File uploaded successfully
        out.println("<br/>File Uploaded Successfully");
    }
    catch (Exception ex) {
        out.println("File Upload Failed due to " + ex);
    }

}
else {
    out.println("--- ---");
}
%>

<%--
else {
    /*sb.append("Sorry this Servlet only handles file upload request");*/
    String action = ( request.getParameter("act") == null ? "" : request.getParameter("act") );
    if (action.equals("proses")) {
        ArrayList data = (ArrayList) session.getAttribute("data");
        String ke = ( session.getAttribute("ke") == null ? "1" : (String)session.getAttribute("ke") );
        sb.append("<span class=\"label label-warning\">Sedang mengupdate Email resmi UM...</span><br/><br/>");
        sb.append("Data ke-<span style=\"font-size:25px;font-weight:bold;\">" + ke + "</span> dari <span style=\"font-size:25px;font-weight:bold;\">" + data.size() + "</span><br/>");
        String user[] = (String[])data.get(Integer.valueOf(ke).intValue() - 1);

        sb.append("<div style=\"float:left;\">");
        sb.append("<table class=\"table table-bordered table-condensed\">");
            sb.append("<tr>");
                sb.append("<td>User</td>");
                sb.append("<td>:</td>");
                sb.append("<td>" + user[0] + "</td>");
            sb.append("</tr>");
            sb.append("<tr>");
                sb.append("<td>Email</td>");
                sb.append("<td>:</td>");
                sb.append("<td>" + user[1] + "</td>");
            sb.append("</tr>");
        sb.append("</table></div>");

        /* update */
        updateEmail(user[0], user[1]);

        if (Integer.valueOf(ke).intValue() == data.size()) {
            response.setHeader("Refresh", "3;URL=" + request.getContextPath() + "/sso/email_resmi/");
            session.setAttribute("pesanEmail", "Data berhasil diupdate!!!");
        }
        else {
            /* set nilai baru */
            int next = Integer.valueOf(ke).intValue() + 1;
            session.setAttribute("ke", Integer.valueOf(next).toString());

            response.setHeader("Refresh", "3;URL=" + request.getContextPath() + "/sso/email_resmi/upload/?act=proses");
        }
    }
}
--%>
