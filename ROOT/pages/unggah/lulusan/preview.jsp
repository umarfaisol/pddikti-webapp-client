<%@page session="true" %>

<%
String table = "lulusan";
%>
<h2>Tabel: <%= table %></h2>
<a href="<%= request.getContextPath() %>/pages/unggah/lulusan/" class="label label-success">Kembali</a><br/>

<%
if(ServletFileUpload.isMultipartContent(request)) {
    Workbook wb = null;

    try {
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for (FileItem item : multiparts) {
            if (!item.isFormField()) {
                String name = item.getName();
                out.println("File: " + name);
                String type = "";
                if (name.toLowerCase().endsWith("xls")) {
                    type = "xls";
                }
                else if (name.toLowerCase().endsWith("xlsx")) {
                    type = "xlsx";
                }
                else {
                    type = "no";
                }
                out.println("<br/>Type: " + type);
                out.println("<br/><span class=\"label label-success\">Preview 10 Data</span>");
                out.println("<table class=\"table table-bordered table-condensed\">");

                /* proses */
                ArrayList listData = new ArrayList();
                if (!type.equals("no")) {
                    wb = WorkbookFactory.create(item.getInputStream());
                    Sheet sheet = wb.getSheetAt(0);

                    int i = 0;
                    for (Row row : sheet) {
                        i++;
                        String nim = ( row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue());
                        String no_seri_ijazah = ( row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue());
                        String tgl_keluar = ( row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue());
                        String ipk = ( row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue());
                        String tgl_sk_yudisium = ( row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue());
                        String sk_yudisium = ( row.getCell(5) == null ? "" : row.getCell(5).getStringCellValue());
                        String jalur_skripsi = ( row.getCell(6) == null ? "" : row.getCell(6).getStringCellValue());
                        String judul_skripsi = ( row.getCell(7) == null ? "" : row.getCell(7).getStringCellValue());
                        String sks_total = ( row.getCell(8) == null ? "" : row.getCell(8).getStringCellValue());

                        String data[] = new String[9];
                        data[0] = nim;
                        data[1] = no_seri_ijazah;
                        data[2] = tgl_keluar;
                        data[3] = ipk;
                        data[4] = tgl_sk_yudisium;
                        data[5] = sk_yudisium;
                        data[6] = jalur_skripsi;
                        data[7] = judul_skripsi;
                        data[8] = sks_total;

                        listData.add(data);
                    }
                }

                /** tes data **/
                listData.remove(0);
                listData.trimToSize();
                session.setAttribute("data", listData);
                session.setAttribute("banyakData", listData.size());

                for (int i = 0; i < listData.size(); i++) {
                    if (i <= 10) {
                        out.println("<tr>");
                        String data[] = (String[])listData.get(i);
                        for (int k = 0; k < data.length; k++) {
                            out.println("<td>" + data[k] + "</td>");
                        }
                        out.println("</tr>");
                    }
                }


                out.println("<tr><td><b>" + new Integer(listData.size()).toString() + "</b> Data " + table + "</td><td>");
                    out.println("<form action=\"" + request.getContextPath() + "/pages/unggah/lulusan/?statusUpload=upload\" method=\"post\">");
                        out.println("<button type=\"submit\" class=\"btn btn-primary\">Proses</button>");
                    out.println("</form>");
                out.println("</td></tr>");
                out.println("</table>");
            }
        }

        //File uploaded successfully
        out.println("<br/>File Uploaded Successfully");
    }
    catch (Exception ex) {
        out.println("File Upload Failed due to " + ex);
    }

}
else {
    out.println("--- ---");
}
%>
