<%
String table = "lulusan";
String statusUpload = ( request.getParameter("statusUpload") == null ? "" : request.getParameter("statusUpload") );

if (statusUpload.equals("upload")) { %>
    <%
    ArrayList data = (ArrayList)session.getAttribute("data");
    String banyakData = ( session.getAttribute("banyakData") == null ? "0" : session.getAttribute("banyakData").toString() );
    String ke = ( session.getAttribute("ke") == null ? "1" : (String)session.getAttribute("ke") );
    %>
    <h3>Sedang mengunggah... [<%= table %>]</h3>
    <span class="label label-success" style="font-size:20px;">Data ke: <b><%= ke %></b> dari <b><%= banyakData %></b></span><br/><br/>

    <%-- tambahi progressbar --%>
    <%
    int intKe = new Integer(ke).intValue();
    int intData = new Integer(banyakData).intValue();
    int persen = (int)((intKe * 100) / intData);
    %>

    <%--<%= persen %>--%>
    <div class="progress">
        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="<%= persen %>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <%= persen %>%;">
            <%= persen %> % selesai
        </div>
    </div>
    <%-- akhir progress --%>

    <a href="<%= request.getContextPath() %>/" class="label label-primary">Kembali</a><br/><br/>
    <table class="table table-bordered table-endorsed">
        <tr>
            <th>No</th>

    <%
        InsertRecord ir = new InsertRecord();
        GetRecordset gr = new GetRecordset();
        UpdateRecord ur = new UpdateRecord();

        String isi[] = (String[])data.get(0);
        String nim = isi[0];
        DefaultTableModel reg_pd = gr.getRecordset(token, "mahasiswa_pt", endpoint, "TRIM(nipd)='" + nim + "'", "", "1", "");
        String id_reg_pd = "";
        if (reg_pd.getRowCount() > 0) {
            id_reg_pd = (String)reg_pd.getValueAt(0, 0);
        }
        out.println("id_reg_pd: " + id_reg_pd);

        /* update */
        if (!id_reg_pd.equals("")) {
            out.println("<br/>ADA -- update");
            Map objKey = new LinkedHashMap();
            objKey.put("id_reg_pd", id_reg_pd);

            Map objData = new LinkedHashMap();
            objData.put("no_seri_ijazah", isi[1]);
            objData.put("tgl_keluar", isi[2]);
            objData.put("ipk", isi[3]);
            objData.put("tgl_sk_yudisium", isi[4]);
            objData.put("sk_yudisium", isi[5]);
            objData.put("jalur_skripsi", isi[6]);
            objData.put("judul_skripsi", isi[7]);
            objData.put("id_jns_keluar", "1");

            Map obj = new LinkedHashMap();
            obj.put("key", objKey);
            obj.put("data", objData);
            String jsonText = JSONValue.toJSONString(obj);
            out.println("<br/>UpdateRecord() -->" + jsonText);

            DefaultTableModel defaultTableModel = ur.updateRecord(token, "mahasiswa_pt", endpoint, jsonText);
            int banyak = defaultTableModel.getRowCount();
            int banyakKolom = defaultTableModel.getColumnCount();
        }

        data.remove(0);
        data.trimToSize();
        session.setAttribute("data", data);

        ke = new Integer(Integer.valueOf(ke).intValue() + 1).toString();
        session.setAttribute("ke", ke);

        if (Integer.valueOf(ke).intValue() <= Integer.valueOf(banyakData).intValue()) {
            out.println("<br/><br/><span class=\"label label-success\">Ke data selanjutnya...</span>");
            response.setHeader("Refresh", "2;URL=" + request.getContextPath() + "/pages/unggah/lulusan/?statusUpload=upload");
        }
        else {
            session.setAttribute("pesan", "Data telah diupload ke tabel " + table);
            response.setHeader("Refresh", "5;URL=" + request.getContextPath() + "/");
        }
        %>
    </table>
<%
}
else {
%>
    <h2>Tabel: <%= table %></h2>
    <a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a>
    <form action="<%= request.getContextPath() %>/pages/unggah/lulusan/?proses=preview" method="post" enctype="multipart/form-data">
        <input type="file" name="berkas" />
        <input type="submit" class="btn btn-primary" value="Unggah" />
    </form>
<%
}
%>
