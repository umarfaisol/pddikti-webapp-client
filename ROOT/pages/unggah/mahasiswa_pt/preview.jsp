<%@page session="true" %>

<%
String table = "mahasiswa_pt";
%>
<h2>Tabel: <%= table %></h2>
<a href="<%= request.getContextPath() %>/pages/unggah/mahasiswa_pt/" class="label label-success">Kembali</a><br/>

<%
//process only if its multipart content
if(ServletFileUpload.isMultipartContent(request)) {
    Workbook wb = null;

    try {
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
        for (FileItem item : multiparts) {
            if (!item.isFormField()) {
                String name = item.getName();
                out.println("File: " + name);
                String type = "";
                if (name.toLowerCase().endsWith("xls")) {
                    type = "xls";/*
                    NPOIFSFileSytem fs = new NPOIFSFileSystem(item.getInputStream());
                    wb = new HSSFWorkbook(fs.getRoot());*/
                }
                else if (name.toLowerCase().endsWith("xlsx")) {
                    type = "xlsx";/*
                    OPCPackage pkg = OPCPackage.open(item.getInputStream());
                    wb = new XSSFWorkbook(pkg);*/
                }
                else {
                    type = "no";
                }
                out.println("<br/>Type: " + type);
                out.println("<br/><span class=\"label label-success\">Preview 10 Data</span>");
                out.println("<table class=\"table table-bordered table-condensed\">");

                /* proses */
                ArrayList listData = new ArrayList();
                if (!type.equals("no")) {
                    wb = WorkbookFactory.create(item.getInputStream());
                    Sheet sheet = wb.getSheetAt(0);

                    int i = 0;
                    for (Row row : sheet) {
                        i++;

                        String nm_pd = ( row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue());
                        String tmpt_lahir = ( row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue());
                        String tgl_lahir = ( row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue());
                        String nm_ibu_kandung = ( row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue());
                        String id_sms = ( row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue());
                        String id_sp = ( row.getCell(5) == null ? "" : row.getCell(5).getStringCellValue());
                        String id_jns_daftar = ( row.getCell(6) == null ? "" : row.getCell(6).getStringCellValue());
                        String tgl_masuk_sp = ( row.getCell(7) == null ? "" : row.getCell(7).getStringCellValue());
                        String nipd = ( row.getCell(8) == null ? "" : row.getCell(8).getStringCellValue());
                        String mulai_smt = ( row.getCell(9) == null ? "" : row.getCell(9).getStringCellValue());

                        String data[] = new String[10];
                        data[0] = nm_pd;
                        data[1] = tmpt_lahir;
                        data[2] = tgl_lahir;
                        data[3] = nm_ibu_kandung;
                        data[4] = id_sms;
                        data[5] = id_sp;
                        data[6] = id_jns_daftar;
                        data[7] = tgl_masuk_sp;
                        data[8] = nipd;
                        data[9] = mulai_smt;

                        listData.add(data);
                    }
                }

                /** tes data **/
                listData.remove(0);
                listData.trimToSize();
                session.setAttribute("data", listData);
                session.setAttribute("banyakData", listData.size());

                for (int i = 0; i < listData.size(); i++) {
                    if (i <= 10) {
                        out.println("<tr>");
                        String data[] = (String[])listData.get(i);
                        for (int k = 0; k < data.length; k++) {
                            out.println("<td>" + data[k] + "</td>");
                        }
                        out.println("</tr>");
                    }
                }


                out.println("<tr><td><b>" + new Integer(listData.size()).toString() + "</b> Data " + table + "</td><td>");
                    out.println("<form action=\"" + request.getContextPath() + "/pages/unggah/mahasiswa_pt/?statusUpload=upload\" method=\"post\">");
                        out.println("<button type=\"submit\" class=\"btn btn-primary\">Proses</button>");
                    out.println("</form>");
                out.println("</td></tr>");
                out.println("</table>");
            }
        }

        //File uploaded successfully
        out.println("<br/>File Uploaded Successfully");
    }
    catch (Exception ex) {
        out.println("File Upload Failed due to " + ex);
    }

}
else {
    out.println("--- ---");
}
%>
