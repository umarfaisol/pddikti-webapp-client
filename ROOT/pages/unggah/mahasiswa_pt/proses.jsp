<%
String table = "mahasiswa_pt";
String statusUpload = ( request.getParameter("statusUpload") == null ? "" : request.getParameter("statusUpload") );

if (statusUpload.equals("upload")) { %>
    <%
    ArrayList data = (ArrayList)session.getAttribute("data");
    String banyakData = ( session.getAttribute("banyakData") == null ? "0" : session.getAttribute("banyakData").toString() );
    String ke = ( session.getAttribute("ke") == null ? "1" : (String)session.getAttribute("ke") );
    %>
    <h3>Sedang mengunggah... [<%= table %>]</h3>
    <span class="label label-success" style="font-size:20px;">Data ke: <b><%= ke %></b> dari <b><%= banyakData %></b></span><br/><br/>
    <a href="<%= request.getContextPath() %>/" class="label label-primary">Kembali</a><br/><br/>
    <table class="table table-bordered table-endorsed">
        <tr>
            <th>No</th>

    <%
        InsertRecord ir = new InsertRecord();
        GetRecordset gr = new GetRecordset();
        UpdateRecord ur = new UpdateRecord();

        String isi[] = (String[])data.get(0);

        /** print isi **/
        for (int i = 0; i < isi.length; i++) {
            out.println("isi[" + i + "] = \t" + isi[i] + "<br/>");
        }

        /* cek */
        String nm_pd = isi[0];
        String tmpt_lahir = isi[1];
        String tgl_lahir = isi[2];
        String nm_ibu_kandung = isi[3];
        String id_sms = isi[4];
        String id_sp = isi[5];
        String id_jns_daftar = isi[6];
        String tgl_masuk_sp = isi[7];
        String nipd = isi[8];
        String mulai_smt = isi[9];

        String id_pd = "";

        /* cari id_pd */
        DefaultTableModel pd = gr.getRecordset(token, "mahasiswa", endpoint, "LOWER(nm_pd) LIKE '%" + nm_pd.toLowerCase() + "%' AND LOWER(tmpt_lahir) LIKE '%" + tmpt_lahir.toLowerCase() + "%' AND tgl_lahir='" + tgl_lahir + "' AND LOWER(nm_ibu_kandung) LIKE '%" + nm_ibu_kandung.toLowerCase() + "%'", "", "1", "");

        /* cek apa ketemu mahasiswa */
        if (pd.getRowCount() > 0) {
            id_pd = (String)pd.getValueAt(0, 0);

            /* cari id_sp */
            DefaultTableModel sp = gr.getRecordset(token, "satuan_pendidikan", endpoint, "npsn='" + id_sp + "'", "", "1", "");

            if (sp.getRowCount() > 0) {
                id_sp = (String)sp.getValueAt(0, 0);
            }

            /* cari id_sms */
            DefaultTableModel sms = gr.getRecordset(token, "sms", endpoint, "TRIM(kode_prodi)='" + id_sms + "' AND id_sp='" + id_sp + "'", "", "1", "");

            if (sms.getRowCount() > 0) {
                id_sms = (String)sms.getValueAt(0, 0);
            }

            /* cari nim */
            DefaultTableModel nim = gr.getRecordset(token, "mahasiswa_pt", endpoint, "TRIM(nipd)='" + nipd + "'", "", "1", "");

            if (nim.getRowCount() > 0) {
                out.println("NIM sudah ada!!!<br/>");
            }
            else {
                out.println("INSERT!!!<br/>");

                Map obj = new LinkedHashMap();
                obj.put("id_sms", id_sms);
                obj.put("id_pd", id_pd);
                obj.put("id_sp", id_sp);
                obj.put("id_jns_daftar", id_jns_daftar);
                obj.put("nipd", nipd);
                obj.put("tgl_masuk_sp", tgl_masuk_sp);
                obj.put("a_pernah_paud", "0");
                obj.put("a_pernah_tk", "0");
                obj.put("mulai_smt", mulai_smt);

                String jsonText = JSONValue.toJSONString(obj);
                out.println("InsertRecord() -->\n" + jsonText + "<br/>");

                DefaultTableModel defaultTableModel = ir.insertRecord(token, "mahasiswa_pt", endpoint, jsonText);
                int banyak = defaultTableModel.getRowCount();
                int banyakKolom = defaultTableModel.getColumnCount();
            }
        } /* -- akhir if cari mahasiswa -- */
        else {
            out.println("<span class\"label label-danger\">Data tak ada</span><br/>");
        }
        out.println(id_pd);

        data.remove(0);
        data.trimToSize();
        session.setAttribute("data", data);

        ke = new Integer(Integer.valueOf(ke).intValue() + 1).toString();
        session.setAttribute("ke", ke);

        if (Integer.valueOf(ke).intValue() <= Integer.valueOf(banyakData).intValue()) {
            out.println("<br/><br/><span class=\"label label-success\">Ke data selanjutnya...</span>");
            response.setHeader("Refresh", "1;URL=" + request.getContextPath() + "/pages/unggah/mahasiswa_pt/?statusUpload=upload");
        }
        else {
            session.setAttribute("pesan", "Data telah diupload ke tabel " + table);
            response.setHeader("Refresh", "10;URL=" + request.getContextPath() + "/pages/unggah/mahasiswa_pt/");
        }
        %>
    </table>
<%
}
else {
%>
    <h2>Tabel: <%= table %></h2>
    <a href="<%= request.getContextPath() %>/" class="label label-success">Kembali</a>
    <form action="<%= request.getContextPath() %>/pages/unggah/mahasiswa_pt/?proses=preview" method="post" enctype="multipart/form-data">
        <input type="file" name="berkas" />
        <input type="submit" class="btn btn-primary" value="Unggah" />
    </form>
<%
}
%>
