<%@include file="/util/import.jsp" %>

<%
String token = ( session.getAttribute("token") == null ? "" : session.getAttribute("token").toString() );
String endpoint = ( session.getAttribute("endpoint") == null ? "" : session.getAttribute("endpoint").toString() );

if (token.equals("")) {
    response.sendRedirect(request.getContextPath() + "/");
}

/* set default endpoint */
if (endpoint.equals("")) {
    endpoint = "http://localhost:8082/ws/live.php";
}
%>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PD-Dikti Feeder Client | <%= token %></title>

        <%-- css --%>
        <%@include file="/util/css.jsp" %>
  </head>
  <body>
    <div class="page-header">
      <h1>PD-Dikti <small>Client</small></h1>
    </div>

    <%
    if (session.getAttribute("pesan") != null) {
        out.println("<span class=\"label label-success\" style=\"text-align: center;\">" + session.getAttribute("pesan") + "</span>");
        session.removeAttribute("pesan");
        session.removeAttribute("ke");
        session.removeAttribute("data");
    }

    String proses = ( request.getParameter("proses") == null ? "" : request.getParameter("proses") );
    if (proses.equals("preview")) {
    %>
        <%@include file="/pages/unggah/mahasiswa_pt/preview.jsp" %>
    <%
    }
    else {
    %>
        <%@include file="/pages/unggah/mahasiswa_pt/proses.jsp" %>
    <%
    }%>

    <%-- js --%>
    <%@include file="/util/js.jsp" %>
  </body>
</html>
