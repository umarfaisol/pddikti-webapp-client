<%@include file="/util/import.jsp" %>

<%
String token = ( session.getAttribute("token") == null ? "" : session.getAttribute("token").toString() );
String endpoint = ( session.getAttribute("endpoint") == null ? "" : session.getAttribute("endpoint").toString() );
String act = ( request.getParameter("act") == null ? "GetRecordset" : request.getParameter("act") );

if (token.equals("")) {
    response.sendRedirect(request.getContextPath() + "/");
}

/* set default endpoint */
if (endpoint.equals("")) {
    endpoint = "http://localhost:8082/ws/live.php";
}
%>

<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PD-Dikti Feeder Client | <%= token %></title>

        <%-- css --%>
        <%@include file="/util/css.jsp" %>
  </head>
  <body>
    <div class="page-header">
      <h1>PD-Dikti <small>Client</small></h1>
    </div>

    <%
    /* get recordset */
    if (act.equals("GetRecordset")) { %>
        <%--<%@include file="/util/GetRecordset.jsp" %>--%>
        <%@include file="/pages/update_mhs/get_mhs.jsp" %>
    <%
    }

    /* update */
    else if (act.equals("update")) { %>
        <%--<%@include file="/util/UpdateRecord.jsp" %>--%>
        <%@include file="/pages/update_mhs/update.jsp" %>
    <%
    }

    /* menu */
    else { %>
        <%@include file="/pages/menu.jsp" %>
    <% } %>

    <%-- js --%>
    <%@include file="/util/js.jsp" %>
  </body>
</html>
