<div class="list-group" style="text-align:center;">
    <!-- Single button -->
    <div class="btn-group">
        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="glyphicon glyphicon-upload"></span> Unggah <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="<%= request.getContextPath() %>/pages/?act=Unggah&table=bobot_nilai">Bobot Nilai</a></li>
            <li><a href="<%= request.getContextPath() %>/pages/?act=Upload&table=mahasiswa">Mahasiswa</a></li>
            <li><a href="<%= request.getContextPath() %>/pages/?act=Upload&table=mahasiswa_pt">Mahasiswa PT</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<%= request.getContextPath() %>/pages/?act=Upload&table=kuliah_mahasiswa">Kuliah Mahasiswa</a></li>
            <li><a href="<%= request.getContextPath() %>/pages/?act=Upload&table=lulusan">Lulusan</a></li>
        </ul>
    </div>

    <!-- Perkuliahan -->
    <div class="btn-group">
        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="glyphicon glyphicon-upload"></span> Perkuliahan <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="<%= request.getContextPath() %>/pages/matakuliah">Matakuliah</a></li>
        </ul>
    </div>

    <a href="<%= request.getContextPath() %>/pages/?act=Mahasiswa" class="btn btn-primary"><span class="glyphicon glyphicon-th-list"></span> Mahasiswa</a>
    <a href="<%= request.getContextPath() %>/pages/?act=ListTable" class="btn btn-primary"><span class="glyphicon glyphicon-th-list"></span> Daftar Tabel</a>
    <a href="<%= request.getContextPath() %>/pages/?act=Logout" class="btn btn-danger"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
</div>
