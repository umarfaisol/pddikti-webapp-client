<%@include file="/util/import.jsp" %>
<%--<%@include file="/util/GetToken.jsp" %>--%>

<%
GetToken ws = new GetToken();
String token = ( session.getAttribute("token") == null ? "" : session.getAttribute("token").toString() );
String act = ( request.getParameter("act") == null ? "" : request.getParameter("act") );
if (act.equals("GetToken")) {
    String endpoint = request.getParameter("endpoint");
    String user = request.getParameter("user");
    String pwd = request.getParameter("pwd");
    token = ws.getToken(user, pwd, endpoint);
    session.setAttribute("endpoint", endpoint);
}
if (!token.equals("")) {
    session.setAttribute("token", token);
    response.sendRedirect(request.getContextPath() + "/pages/");
}
%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login | PD-Dikti Feeder Client | <%= token %></title>

    <%-- css --%>
    <%@include file="/util/css.jsp" %>

    <%-- js --%>
    <%@include file="/util/js.jsp" %>
  </head>
  <body>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Login | PD-Dikti Feeder Client | <%= token %></h1>
            <div class="account-wall">
                <img class="profile-img" src="<%= request.getContextPath() %>/gambar/logoUM.jpg"
                    alt="logo UM" style="width:120px;height:120px;">
                <form class="form-signin" method="post">
                    <input type="hidden" name="act" value="GetToken" />
                    <input type="text" name="endpoint" class="form-control" placeholder="Web service Endpoint" value="http://localhost:8082/ws/live.php" required="required" />
                    <input type="text" name="user" class="form-control" placeholder="User"  required="required" autofocus="autofocus" />
                    <input type="password" name="pwd" class="form-control" placeholder="Password" required="required" />
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
                </form>
            </div>
        </div>
    </div>
</div>

  </body>
</html>
