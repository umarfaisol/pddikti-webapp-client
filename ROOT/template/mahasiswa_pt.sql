-- Table: pddikti.mahasiswa_pt

-- DROP TABLE pddikti.mahasiswa_pt;

CREATE TABLE pddikti.mahasiswa_pt
(
  id_reg_pd uuid NOT NULL,
  id_sms uuid,
  id_pd uuid,
  id_sp uuid,
  id_jns_daftar numeric(1,0),
  nipd character varying(18),
  tgl_masuk_sp date,
  id_jns_keluar character(1),
  tgl_keluar date,
  ket character varying(128),
  skhun character(20),
  a_pernah_paud numeric(1,0),
  a_pernah_tk numeric(1,0),
  mulai_smt character varying(5),
  sks_diakui numeric(3,0),
  jalur_skripsi numeric(1,0),
  judul_skripsi character varying(250),
  bln_awal_bimbingan date,
  bln_akhir_bimbingan date,
  sk_yudisium character varying(40),
  tgl_sk_yudisium date,
  ipk double precision,
  no_seri_ijazah character varying(40),
  sert_prof character varying(40),
  a_pindah_mhs_asing numeric(1,0),
  nm_pt_asal character varying(50),
  nm_prodi_asal character varying(50),
  CONSTRAINT pk_reg_pd PRIMARY KEY (id_reg_pd)
)
WITH (
  OIDS=FALSE
);
