﻿SELECT
  UPPER(m.mhs_nm) AS nm_pd,
  UPPER(k.kota_nm) AS tmpt_lahir,
  d.mhs_tgllhr AS tgl_lahir,
  UPPER(cd.cmhs_nm_ibu) AS nm_ibu_kandung,
  p.pro_dikti AS id_sms,
  '001033' AS id_sp,
  '1' AS id_jns_daftar,
  r.reg_tgl AS tgl_masuk_sp,
  m.mhs_nim AS nipd,
  '20161' AS mulai_smt
FROM
  dtum.m_mhs m,
  dtum.m_mhs_det d,
  dtum.t_reg r,
  dtum.m_prodi p,
  dtum.m_kab k,
  dtum.m_cmhs_det cd
WHERE
  (m.mhs_unix=d.mhs_unix) AND
  (m.pro_kd=p.pro_kd) AND
  (m.mhs_unix=r.mhs_unix) AND
  (d.mhs_kotalhr=k.kota_kd) AND
  (m.mhs_nodft=cd.cmhs_nodft) AND
  (r.reg_thaka='20151') AND
  (m.mhs_tahun='2015') AND
  (p.pro_dikti = '45102')
