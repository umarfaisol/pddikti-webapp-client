-- Table: pddikti.t_lulus_20091

-- DROP TABLE pddikti.t_lulus_20091;

CREATE TABLE pddikti.t_lulus_20091
(
  nim character varying(12) NOT NULL,
  no_seri_ijazah character varying(50),
  tgl_keluar date NOT NULL DEFAULT now(),
  ipk double precision,
  tgl_sk_yudisium date NOT NULL DEFAULT now(),
  sk_yudisium character varying(40),
  jalur_skripsi numeric NOT NULL DEFAULT 0,
  judul_skripsi character varying(250),
  sks_total integer NOT NULL DEFAULT 0,
  CONSTRAINT pk_lulus_20091 PRIMARY KEY (nim)
)
WITH (
  OIDS=FALSE
);
