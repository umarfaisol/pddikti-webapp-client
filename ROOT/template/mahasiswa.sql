-- Table: pddikti.mahasiswa

-- DROP TABLE pddikti.mahasiswa;

CREATE TABLE pddikti.mahasiswa
(
  id_pd uuid NOT NULL,
  nm_pd character varying(60),
  jk character(1),
  nisn character(10),
  nik character(16),
  tmpt_lahir character varying(32),
  tgl_lahir date,
  id_agama smallint,
  id_kk integer,
  id_sp uuid,
  jln character varying(80),
  rt numeric(2,0),
  rw numeric(2,0),
  nm_dsn character varying(50),
  ds_kel character varying(50),
  id_wil character(8),
  kode_pos character(5),
  id_jns_tinggal numeric(2,0),
  id_alat_transport numeric(2,0),
  telepon_rumah character varying(20),
  telepon_seluler character varying(20),
  email character varying(50),
  a_terima_kps numeric(1,0),
  no_kps character varying(40),
  stat_pd character(1),
  nm_ayah character varying(60),
  tgl_lahir_ayah date,
  id_jenjang_pendidikan_ayah numeric(2,0),
  id_pekerjaan_ayah integer,
  id_penghasilan_ayah integer,
  id_kebutuhan_khusus_ayah integer,
  nm_ibu_kandung character varying(60),
  tgl_lahir_ibu date,
  id_jenjang_pendidikan_ibu numeric(2,0),
  id_penghasilan_ibu integer,
  id_pekerjaan_ibu integer,
  id_kebutuhan_khusus_ibu integer,
  nm_wali character varying(30),
  tgl_lahir_wali date,
  id_jenjang_pendidikan_wali numeric(2,0),
  id_pekerjaan_wali integer,
  id_penghasilan_wali integer,
  kewarganegaraan character(2),
  CONSTRAINT pk_pd PRIMARY KEY (id_pd)
)
WITH (
  OIDS=FALSE
);
