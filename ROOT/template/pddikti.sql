﻿SELECT * FROM pddikti.akm_20141 WHERE (id_smt='20141') AND (nim IN(
            SELECT
              g.mhs_nim
            FROM
              dtum.g_krs_20141 g,
              dtum.m_mhs m,
              dtum.m_prodi p
            WHERE
              (g.mhs_nim=m.mhs_unix) AND
              (m.pro_kd=p.pro_kd) AND
              (p.pro_dikti <> '') AND
              (p.pro_dikti <> '00000') AND
              (p.pro_dikti='83211')
            GROUP BY
              g.mhs_nim))
              

SELECT nim FROM pddikti.akm_20141 WHERE (nim IN( 
        SELECT  
          g.mhs_nim  
        FROM  
          dtum.g_krs_20141  g,  
          dtum.m_mhs m,  
          dtum.m_prodi p  
        WHERE  
          (g.mhs_nim=m.mhs_unix) AND  
          (m.pro_kd=p.pro_kd) AND  
          (p.pro_dikti <> '') AND  
          (p.pro_dikti <> '00000') AND  
          (p.pro_dikti='83211')  
        GROUP BY  
          g.mhs_nim))
          

SELECT
  pro_dikti,
  jjg_kd || ' ' || pro_nm AS nama
FROM
  dtum.m_prodi
WHERE
  (pro_dikti <> '') AND
  (pro_dikti <> '00000')

/* nim per prodi */
SELECT
  g.mhs_nim
FROM
  dtum.g_krs_20141 g,
  dtum.m_mhs m,
  dtum.m_prodi p
WHERE
  (g.mhs_nim=m.mhs_unix) AND
  (m.pro_kd=p.pro_kd) AND
  (p.pro_dikti <> '') AND
  (p.pro_dikti <> '00000') AND
  (p.pro_dikti='61101')
GROUP BY
  g.mhs_nim

/** ip semester **/
SELECT
  (SUM(g.nilai_a * kd.mt_sks) / SUM(kd.mt_sks)) AS ip
FROM
  dtum.g_krs_20141 g,
  dtum.t_kdgen kd  
WHERE
  (mhs_nim='140413807269') AND
  (g.kd_komp=kd.kd_komp) AND
  (kd.thaka='20141')

/** ipk **/
SELECT AVG(ip)
FROM
  dna.ip_2012
WHERE
  mhs_nim='120221521983' AND thaka <= '20132'

