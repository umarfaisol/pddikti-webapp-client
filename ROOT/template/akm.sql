-- Table: pddikti.akm_20091

-- DROP TABLE pddikti.akm_20091;

CREATE TABLE pddikti.akm_20091
(
  id_smt character(5) NOT NULL,
  nim character(12) NOT NULL,
  ips numeric NOT NULL DEFAULT 0,
  sks_smt integer NOT NULL DEFAULT 0,
  ipk numeric NOT NULL DEFAULT 0,
  sks_total integer NOT NULL DEFAULT 0,
  id_stat_mhs character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT pk_akm_20091 PRIMARY KEY (nim)
)
WITH (
  OIDS=FALSE
);
