﻿-- select mahasiswa
SELECT
  UPPER(m.mhs_nm) AS nm_pd,
  (CASE WHEN m.mhs_klm='W' THEN 'P' ELSE UPPER(m.mhs_klm) END) AS jk,
  m.nik AS nik,
  UPPER(k.kota_nm) AS tmpt_lahir,
  d.mhs_tgllhr AS tgl_lahir,
  a.kode_dikti AS id_agama,
  '0' AS id_kk,
  d.mhs_alamat_mlg AS jln,
  d.mhs_rt AS rt,
  d.mhs_rw AS rw,
  '' AS nm_dsn,
  UPPER(d.mhs_kelurahan) AS ds_kel,
  '050000' AS id_wil,
  '00000' AS kode_pos,
  '0' AS a_terima_kps,
  UPPER(cd.cmhs_nm_ayah) AS nm_ayah,
  '0' AS id_kebutuhan_khusus_ayah,
  UPPER(cd.cmhs_nm_ibu) AS nm_ibu_kandung,
  '0' AS id_kebutuhan_khusus_ibu,
  'ID' AS kewarganegaraan
FROM
  dtum.m_mhs m,
  dtum.m_prodi p,
  dtum.m_mhs_det d,
  dtum.m_kab k,
  dtum.m_agm a,
  dtum.m_cmhs_det cd
WHERE
  (m.pro_kd=p.pro_kd) AND
  (m.mhs_unix=d.mhs_unix) AND
  (d.mhs_kotalhr=k.kota_kd) AND
  (d.mhs_agm=a.agm_kd) AND
  (m.mhs_nodft=cd.cmhs_nodft) AND
  (m.mhs_tahun='2016')
